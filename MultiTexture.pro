TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += .
INCLUDEPATH += /usr/local/include/GLFW

INCLUDEPATH += /home/sajjad/Downloads/GLI/gli-0.5.1.1

LIBS += -lglfw -lGLEW -lGL -lGLU -lSOIL

SOURCES += main.cpp \
    GLSLShader.cpp

HEADERS += \
    GLSLShader.h

