// Include standard headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#define GLFW_INCLUDE_GLU
#include <glfw3.h>

#include "GLSLShader.h"

// Include GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <gli/gli.hpp>

using namespace glm;

using namespace std;

#define GL_CHECK_ERRORS assert(glGetError() == GL_NO_ERROR)
#define BUFFER_OFFSET(offset) ((void*) (offset))

static void error_callback(int error, const char* description);
static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
static void scroll_callback(GLFWwindow* window, double x, double y);


GLFWwindow *window = NULL;
GLSLShader *multitextureShader = NULL;

int winWidth = 512;
int winHeight = 512;

float aspect;

//hold the id for the buffer object
GLuint quadVboID;

//hold the id for the vertex array object
GLuint vaoID;

//hold the id for the first texture
GLuint texID1;
//hold the if for the second texture
GLuint texID2;


void startup();
void shutdown();
void render(float);
void loadShaders();
void loadGeometry();


int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Multi-texturing Demo", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetScrollCallback(window,scroll_callback);



    //make the current window context current
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile

    if (glewInit() != GLEW_OK)
    {
       fprintf(stderr, "Failed to initialize GLEW\n");
       exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
       std::cerr << "OpenGL version 4.4 is yet to be supported. " << std::endl;
       exit(1);
    }

    while(glGetError() != GL_NO_ERROR) {}

    //print out information aout the graphics driver
    std::cout << std::endl;
    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW Verion: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;


    startup();

    do{
       render(static_cast<float>(glfwGetTime()));
       // Swap buffers
       glfwSwapBuffers(window);
       glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
       glfwWindowShouldClose(window) == 0 );


    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    shutdown();

    exit(EXIT_SUCCESS);
}


void startup()
{
    //load the shaders and compile them
    loadShaders();

    loadGeometry();
}

void shutdown()
{
    glDeleteBuffers(1,&quadVboID);

    glDeleteTextures(1,&texID1);
    glDeleteTextures(1,&texID2);

    glDeleteVertexArrays(1,&vaoID);
}

void render(float time)
{
    //float t = float(time & 0x3FFF)/float(0x3FFF);
    float t = time;

    glClearColor(0.0f,1.0f,0.0f,1.0f);
    glClearDepth(1.0f);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDisable(GL_CULL_FACE);

    multitextureShader->Use();

    glUniform1f((*multitextureShader)("time"),t);

    glBindVertexArray(vaoID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,texID1);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,texID2);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    multitextureShader->UnUse();
}

void loadShaders()
{
    if(multitextureShader)
    {
        multitextureShader->DeleteShaderProgram();
        delete multitextureShader;
        multitextureShader = NULL;
    }

    multitextureShader = new GLSLShader();

    multitextureShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/multitexture.vert");
    multitextureShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/multitexture.frag");

    multitextureShader->CreateAndLinkProgram();

    multitextureShader->Use();

    multitextureShader->AddAttribute("vPosition");
    multitextureShader->AddAttribute("vTexCoord");

    multitextureShader->AddUniform("time");
    multitextureShader->AddUniform("texSampler1");
    multitextureShader->AddUniform("texSampler2");

    multitextureShader->UnUse();

}


void loadGeometry()
{
    glGenBuffers(1,&quadVboID);
    glBindBuffer(GL_ARRAY_BUFFER,quadVboID);

    //define the quad data
    static const GLfloat quad_data[] =
    {
         1.0f, -1.0f, // vertex position for the quad
        -1.0f, -1.0f,
        -1.0f, 1.0f,
         1.0f, 1.0f,

         0.0f, 0.0f, // texture coordinate for the quad
         1.0f, 0.0f,
         1.0f, 1.0f,
         0.0f, 1.0f
    };

    glBufferData(GL_ARRAY_BUFFER,sizeof(quad_data),quad_data,GL_STATIC_DRAW);

    glGenVertexArrays(1,&vaoID);
    glBindVertexArray(vaoID);

    //the following two functions will enable to automatically fill the first attribute
    //in the vertex shader with data it has read from the buffer that was bound when glVertexAttribPointer()
    //was called
    glVertexAttribPointer((*multitextureShader)["vPosition"],2,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));
    glVertexAttribPointer((*multitextureShader)["vTexCoord"],2,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(8 * sizeof(float)));

    //the following two functions tells to the OpenGL to use the data in the buffer
    //to fill the vertex attribute rather than using the data we give it using one of the
    //glVertexAttrib*() functions
    glEnableVertexAttribArray((*multitextureShader)["vPosition"]);
    glEnableVertexAttribArray((*multitextureShader)["vTexCoord"]);

    multitextureShader->Use();

    //send the texture sampler earlier during the initialization
    glUniform1i((*multitextureShader)("texSampler1"),0);
    glUniform1i((*multitextureShader)("texSampler2"),1);

    multitextureShader->UnUse();


    //now load the textures
    gli::storage Storage(gli::load_dds("images/test.dds"));

    assert(!Storage.empty());

    GLenum target = Storage.layers() > 1 ? GL_TEXTURE_2D_ARRAY : GL_TEXTURE_2D;

    glGenTextures(1,&texID1);
    glBindTexture(target,texID1);

    gli::texture2D texture(Storage);

    glTexStorage2D(target,static_cast<int>(texture.levels()),
                   static_cast<GLenum>(gli::internal_format(texture.format())),
                   static_cast<GLsizei>(texture.dimensions().x),
                   static_cast<GLsizei>(texture.dimensions().y));


    for(gli::texture2D::size_type Level = 0 ; Level < texture.levels(); ++Level)
    {
        glTexSubImage2D(target,
                        static_cast<GLint>(Level),0,0,
                        static_cast<GLsizei>(texture[Level].dimensions().x),
                        static_cast<GLsizei>(texture[Level].dimensions().y),
                        static_cast<GLenum>(gli::external_format(texture.format())),
                        static_cast<GLenum>(gli::type_format(texture.format())),
                        texture[Level].data());
    }

    //set some of the texture parameters
    glTexParameteri(target,GL_TEXTURE_BASE_LEVEL,0);
    glTexParameteri(target,GL_TEXTURE_MAX_LEVEL,static_cast<GLint>(texture.levels() - 1));
    glTexParameteri(target,GL_TEXTURE_MIN_FILTER,texture.levels() > 1 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
    glTexParameteri(target,GL_TEXTURE_MAG_FILTER,GL_LINEAR);



    gli::storage Storage1(gli::load_dds("images/test3.dds"));

    assert(!Storage1.empty());

    GLenum target1 = Storage1.layers() > 1 ? GL_TEXTURE_2D_ARRAY : GL_TEXTURE_2D;

    glGenTextures(1,&texID2);
    glBindTexture(target1,texID2);

    gli::texture2D texture1(Storage1);

    glTexStorage2D(target1,static_cast<int>(texture1.levels()),
                   static_cast<GLenum>(gli::internal_format(texture1.format())),
                   static_cast<GLsizei>(texture1.dimensions().x),
                   static_cast<GLsizei>(texture1.dimensions().y));


    for(gli::texture2D::size_type Level = 0 ; Level < texture1.levels(); ++Level)
    {
        glTexSubImage2D(target,
                        static_cast<GLint>(Level),0,0,
                        static_cast<GLsizei>(texture1[Level].dimensions().x),
                        static_cast<GLsizei>(texture1[Level].dimensions().y),
                        static_cast<GLenum>(gli::external_format(texture1.format())),
                        static_cast<GLenum>(gli::type_format(texture1.format())),
                        texture1[Level].data());
    }

    //set some of the texture parameters
    glTexParameteri(target1,GL_TEXTURE_BASE_LEVEL,0);
    glTexParameteri(target1,GL_TEXTURE_MAX_LEVEL,static_cast<GLint>(texture1.levels() - 1));
    glTexParameteri(target1,GL_TEXTURE_MIN_FILTER,texture1.levels() > 1 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
    glTexParameteri(target1,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    if(height < 1)
        height = 1;

    winWidth = width;
    winHeight = height;

    glViewport(0,0,winWidth,winHeight);
}

void scroll_callback(GLFWwindow* window, double x, double y)
{

}

void error_callback(int error, const char* description)
{

}


