#version 430 core

//get the vertex position 
//and texture coordinates
//are sent as vertex attributes
in vec2 vPosition;
in vec2 vTexCoord;

//generate texture coordinates
//as varying variables and sent
//to the fragment shader
out vec2 texCoord1;
out vec2 texCoord2;

uniform float time; 

void main()
{
	const mat2 m = mat2(vec2(cos(time),sin(time)),
			    vec2(-sin(time),cos(time)));

	gl_Position = vec4(vPosition,0.5,1.0);			    
	texCoord1 = vTexCoord * m;
	texCoord2 = vTexCoord * transpose(m);
}