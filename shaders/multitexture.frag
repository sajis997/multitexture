#version 430 core

in vec2 texCoord1;
in vec2 texCoord2;

out vec4 color;

uniform sampler2D texSampler1;
uniform sampler2D texSampler2;

void main()
{
	color = texture(texSampler1,texCoord1) + 
	      	texture(texSampler2,texCoord2);
}